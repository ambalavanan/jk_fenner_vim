// Call the dataTables jQuery plugin
$(document).ready(function() {
  $('#dataTable').DataTable({
              searching: false,
              paging: false,
              info: false,
    columnDefs: [{
            orderable: false,
            className: 'select-checkbox',
            targets: 0
        }],
        select: {
            style: 'os',
            selector: 'td:first-child'
        },
  });
  $('#datepicker').datepicker({
      uiLibrary: 'bootstrap4'
  });
   $('#datepicker1').datepicker({
       uiLibrary: 'bootstrap4'
   });
});
