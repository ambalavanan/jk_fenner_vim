@extends('layouts.theme')
@section('content')
 
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                           <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 nopadding  print-shape">
                                <div class="toll-free">
                                      <span class="toll-free"> Print Invoice </span>
                                </div>
                            </div>
                            <!-- <div id="triangle-bottomleft">
                               
                            </div>  -->
                        <div class="card mb-4 mt-2">
                            
                            <div class="card-body">
                                    <div class="col-md-12 border p-3">
                                        <div class="row vim">
                                            <div class="col-md-3">
                                                <input id="datepicker"  placeholder="From date"/>
                                            </div>
                                            <div class="col-md-3">
                                                <input id="datepicker1"  placeholder="To date" />
                                            </div>
                                            <div class="col-md-6 mt-2 code">
                                                <p><span class="secondary p-2 ">Invoice To</span><span class="secondary1 p-2 pr-5">Hero Motocorp Ltd</span><span class="secondary p-2 pr-5">Cust.Code</span><span class="secondary1 p-2 pr-5">HMIL</span></p>
                                            </div>
                                        </div>
                                         <div class="row mt-2 vim1">
                                            <div class="col-md-3">
                                               <div class="form-group">
                                                   <div class="input-group mb-3">
                                                    <input type="text" class="form-control" placeholder="Enter Invoice No" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" id="basic-addon2"><i class="fas fa-file-invoice"></i></span>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-5 mt-3">
                                                <div class="checkbox">
                                                    <input type="checkbox" id="check" name="check" value="" />
                                                    <label for="check">
                                                    <span></span>All Copies
                                                    </label> 
                                                    <span>
                                                         <input type="checkbox" id="check" name="check" value="" />
                                                            <label for="check">
                                                            <span></span>Original
                                                            </label>
                                                    </span> 
                                                    <span>
                                                          <input type="checkbox" id="check" name="check" value="" />
                                                            <label for="check">
                                                            <span></span>Duplicate
                                                            </label>
                                                    </span>  
                                                    <span>
                                                         <input type="checkbox" id="check" name="check" value="" />
                                                            <label for="check">
                                                            <span></span>Triplicate
                                                            </label>
                                                    </span>
                                                    <span>
                                                          <input type="checkbox" id="check" name="check" value="" />
                                                            <label for="check">
                                                            <span></span>Extra
                                                            </label>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                             <div class=" row button">
                                                <button type="button" class=" btn btn-success btn-sm pl-4 pr-4">Print</button>
                                                <button type="button" class=" btn btn-danger btn-sm ml-2">Download</button>
                                                <button type="button" class="btn btn-info btn-sm ml-2 pl-4 pr-4">Reset</button>
                                                <button type="button" class="btn btn-warning btn-sm text-white ml-2 pl-4 pr-4 bt_cancel">Cancel</button>
                                            </div>
                                            </div>
                                            <!-- <div class="col-md-2">
                                                <div class="exp mt-2">
                                                    <div class="checkbox">
                                                            <input type="checkbox" id="check" name="check" value="" />
                                                            <label for="check">
                                                            <span></span>All Copies
                                                            </label>
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="col-md-2">
                                                <div class="exp mt-2 ch-1">
                                                    <div class="checkbox">
                                                        <div>
                                                            <input type="checkbox" id="check" name="check" value="" />
                                                            <label for="check">
                                                            <span></span>Original
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="col-md-2">
                                                <div class="exp mt-2 ch-3">
                                                    <div class="checkbox">
                                                            <input type="checkbox" id="check" name="check" value="" />
                                                            <label for="check">
                                                            <span></span>Duplicate
                                                            </label>
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="col-md-2">
                                                <div class="exp mt-2 ch-4">
                                                    <div class="checkbox">
                                                            <input type="checkbox" id="check" name="check" value="" />
                                                            <label for="check">
                                                            <span></span>Triplicate
                                                            </label>
                                                    </div>
                                                </div> -->
                                            </div>
                                            <!-- <div class="col-md-2">
                                                <div class="exp mt-2 ch-5">
                                                    <div class="checkbox">
                                                            <input type="checkbox" id="check" name="check" value="" />
                                                            <label for="check">
                                                            <span></span>Extra
                                                            </label>
                                                    </div>
                                                </div>
                                            </div> -->
                                           
                                            
                                         </div>

                             
                            </div>
                               <div class="table-responsive mt-4">
                                    <table class="table table-bordered table table-striped" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <td></td>
                                                <th>Inv.No</th>
                                                <th>Inv. Date</th>
                                                <th>Other Amt</th>
                                                <th>Discount Amt</th>
                                                <th>Tax Amt</th>
                                                <th>Item Tot</th>
                                                <th>Grand Totl.Amt</th>
                                                <th>Created By</th>
                                                <th>Created At</th>
                                                 <th>Pdf.Status</th>
                                                  <th>Actions</th>
                                            </tr>
                                        </thead>
                                       
                                        <tbody>
                                            <tr>
                                                <td></td>
                                                <td>Tiger Nixon</td>
                                                <td>System Architect</td>
                                                <td>Edinburgh</td>
                                                <td>61</td>
                                                <td>2011/04/25</td>
                                                <td>$320,800</td>
                                                <td>2011/07/25</td>
                                                <td>$170,750</td>
                                                 <td>$170,750</td>
                                                  <td>$170,750</td>
                                                 <td> <button type="button" class="btn btn-success btn-sm"><i class="fas fa-eye"></i></button>
                                                <button type="button" class="btn btn-danger btn-sm"><i class="fas fa-download"></i></button></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>Garrett Winters</td>
                                                <td>Accountant</td>
                                                <td>Tokyo</td>
                                                <td>63</td>
                                                <td>2011/07/25</td>
                                                <td>$170,750</td>
                                                <td>2011/07/25</td>
                                                <td>$170,750</td>
                                                 <td>$170,750</td>
                                                  <td>Undesigned</td>
                                                  <td> <button type="button" class="btn btn-success btn-sm"><i class="fas fa-eye"></i></button>
                                                <button type="button" class="btn btn-danger btn-sm"><i class="fas fa-download"></i></button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                        </div>
                    </div>
                </main>
                <!-- <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2019</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer> -->
            </div>

@endsection