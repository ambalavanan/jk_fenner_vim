@extends('layouts.theme')
@section('content')
    
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <div class="row four-grids mt-5">
                <div class="col-md-3 four-grid">
                    <div class="four-grid1">
                        <span class="icon four-grid1">
                            <i class="fas fa-chart-line "></i>
                        </span>
                        <div class="four-text">
                            <h3>Total Sales Invoices</h3>
                            <h4>3</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 four-grid">
                    <div class="four-grid2">
                        <span class="icon four-grid2">
                            <i class="fas fa-shopping-cart"></i>
                        </span>
                        <div class="four-text">
                            <h3>Total PO List</h3>
                            <h4>8</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 four-grid">
                    <div class="four-grid3">
                        <span class="icon four-grid3">
                            <i class="fas fa-shopping-cart"></i>
                        </span>
                        <div class="four-text">
                            <h3>Invoice Raised PO</h3>
                            <h4>6</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 four-grid">
                    <div class="four-grid4">
                        <span class="icon four-grid4">
                            <i class="fas fa-shopping-cart"></i>
                        </span>
                        <div class="four-text">
                            <h3>Invoice Unraised PO</h3>
                            <h4>2</h4>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row four-grids">
                <div class="col-md-3 four-grid">
                    <div class="four-grid1">
                        <span class="icon four-grid1">
                          <i class="fab fa-product-hunt"></i>
                        </span>
                        <div class="four-text">
                            <h3>Total Products</h3>
                            <h4>3</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 four-grid">
                    <div class="four-grid2">
                        <span class="icon four-grid2">
                          <i class="far fa-user-circle"></i>
                        </span>
                        <div class="four-text">
                            <h3>Total Customers</h3>
                            <h4>8</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 four-grid">
                    <div class="four-grid3">
                        <span class="icon four-grid3">
                            <i class="fas fa-users"></i>
                        </span>
                        <div class="four-text">
                            <h3>Total Employees</h3>
                            <h4>6</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 four-grid">
                    <div class="four-grid4">
                        <span class="icon four-grid4">
                           <i class="fas fa-user-tie"></i>
                        </span>
                        <div class="four-text">
                            <h3>Total Authorized Users</h3>
                            <h4>2</h4>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>  
        </div>   
    </main>
</div>

@endsection