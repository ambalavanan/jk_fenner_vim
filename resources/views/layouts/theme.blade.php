<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Dashboard - VIM</title>
         <link href="{{asset('assets/css/style.css') }}" rel="stylesheet" />
        <link href="{{asset('assets/css/bootstrap.css') }}" rel="stylesheet" />
        <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" crossorigin="anonymous" />
        <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
        <link href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css" rel="stylesheet" type="text/css">
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-blue">
            <a class="navbar-brand" href="index.html"><img src="{{asset('assets/images/Vim.png')}}"></a>
            <form class="search-bar d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <div class="input-group">
                    <input class="" type="text" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button"><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </form>
            <ul class="navbar-nav ml-auto ml-md-0">
                <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                <span class="badge badge-danger badge-counter">3+</span>
              </a>
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                  Alerts Center
                </h6>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-primary">
                      <i class="fas fa-file-alt text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 12, 2019</div>
                    <span class="font-weight-bold">A new monthly report is ready to download!</span>
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-success">
                      <i class="fas fa-donate text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 7, 2019</div>
                    $290.29 has been deposited into your account!
                  </div>
                </a>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="mr-3">
                    <div class="icon-circle bg-warning">
                      <i class="fas fa-exclamation-triangle text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">December 2, 2019</div>
                    Spending Alert: We've noticed unusually high spending for your account.
                  </div>
                </a>
                <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
              </div>
            </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="{{route('login')}}">Logout</a>
                    </div>
                </li>
            </ul>
        </nav>
         <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion  bg-white" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="user-panel">
                                <div class="float-left image">
                                <img src="{{asset('assets/images/user2-160x160.jpg')}}" class="rounded-circle" alt="User Image">
                                </div>
                                <div class="float-left info">
                                    <p>Welcome</p>
                                    <a href="#"><i class="fa fa-circle text-success"></i> Admin</a>
                                </div>
                            </div>
                            <a class="nav-link link" href="{{route('dashboard')}}">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Dashboard</a>
                                  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts"
                                ><div class="sb-nav-link-icon"><i class="fas fa-sitemap"></i></div>
                                Master
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div
                            ></a>
                            <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="layout-static.html"><div class="sb-nav-link-icon">
                                           <i class="fas fa-industry"></i>
                                        </div>Company
                                    </a>
                                    <a class="nav-link" href="layout-sidenav-light.html"> 
                                        <div class="sb-nav-link-icon">
                                           <i class="fas fa-users"></i>
                                        </div>Employee
                                    </a>
                                    <a class="nav-link" href="layout-sidenav-light.html">
                                        <div class="sb-nav-link-icon">
                                           <i class="fas fa-user-tie"></i>
                                        </div>Customer

                                    </a>
                                    <a class="nav-link" href="layout-sidenav-light.html">
                                    <div class="sb-nav-link-icon">
                                        <i class="fas fa-user-tie"></i>
                                    </div>    
                                    Transports</a>
                                    <a class="nav-link" href="layout-sidenav-light.html">
                                    <div class="sb-nav-link-icon">
                                          <i class="fab fa-product-hunt"></i>
                                    </div>    
                                    Part Master</a>
                                    <a class="nav-link" href="layout-sidenav-light.html">
                                    <div class="sb-nav-link-icon">
                                        <i class="fas fa-percent"></i>
                                    </div>    
                                    Rate Master</a>
                                    <a class="nav-link" href="layout-sidenav-light.html">
                                    <div class="sb-nav-link-icon">
                                        <i class="far fa-file"></i>
                                    </div>    
                                    Dropdown Options</a>
                                </nav>
                            </div>
                            <a class="nav-link link" href="#">
                                <div class="sb-nav-link-icon"><i class="fas fa-file-invoice"></i></div>
                                e-Waybill
                            </a>
                             <a class="nav-link link" href="#">
                                <div class="sb-nav-link-icon"><i class="fas fa-file-signature"></i></div>
                                Digital Signer
                            </a>
                             <div class="sb-sidenav-menu-heading">Transaction</div>
                                <a class="nav-link link" href="tables.html"
                                    ><div class="sb-nav-link-icon"><i class="fas fa-truck-moving"></i></div>
                                    Purchase Order</a>
                                <a class="nav-link link" href="tables.html"
                                    ><div class="sb-nav-link-icon"><i class="fas fa-digital-tachograph"></i></div>
                                Sales</a>

                            <div class="sb-sidenav-menu-heading">Print</div>
                                <a class="nav-link link" href="{{route('print_invoice')}}"
                                    ><div class="sb-nav-link-icon"><i class="fas fa-user-friends"></i></div>
                                    Print Invoices </a>
                            <div class="sb-sidenav-menu-heading">Reports</div>
                                 <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts1" aria-expanded="false" aria-controls="collapseLayouts"
                                ><div class="sb-nav-link-icon">   <i class="fas fa-file-alt"></i></div>
                                Reports
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div
                            ></a>
                            <div class="collapse" id="collapseLayouts1" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="layout-static.html">
                                         <div class="sb-nav-link-icon">
                                          <i class="fas fa-user"></i>
                                    </div> 
                                    Customerwise Report</a>
                                    <a class="nav-link" href="layout-sidenav-light.html">
                                         <div class="sb-nav-link-icon">
                                          <i class="fas fa-cart-plus"></i>
                                    </div> 
                                        Productwise Reports</a>
                                    <a class="nav-link" href="layout-sidenav-light.html">
                                         <div class="sb-nav-link-icon">
                                       <i class="fas fa-file-alt"></i>
                                    </div> 
                                    Sales Reports</a>
                                    <a class="nav-link" href="layout-sidenav-light.html">
                                         <div class="sb-nav-link-icon">
                                      <i class="fas fa-money-bill-wave"></i>
                                    </div> 
                                    Activity Reports</a>
                                    <a class="nav-link" href="layout-sidenav-light.html">
                                         <div class="sb-nav-link-icon">
                                      <i class="fas fa-money-bill"></i>
                                    </div> 
                                    Log Reports</a>
                            </div>
                             <div class="sb-sidenav-menu-heading">Settings</div>
                                 <a class="nav-link link" href="tables.html"
                                ><div class="sb-nav-link-icon"><i class="fas fa-envelope"></i></div>
                                Change Password</a>
                                 <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts2" aria-expanded="false" aria-controls="collapseLayouts"
                                ><div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                                User Management
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div
                            ></a>
                            <div class="collapse" id="collapseLayouts2" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="layout-static.html">
                                        <a class="nav-link" href="layout-sidenav-light.html">
                                         <div class="sb-nav-link-icon">
                                            <i class="far fa-user"></i>
                                        </div>
                                    Roles</a>
                                    <a class="nav-link" href="layout-sidenav-light.html">
                                        <div class="sb-nav-link-icon">
                                           <i class="fas fa-users"></i>
                                        </div>
                                    User Creation</a>
                                </nav>
                            </div>
                        </div>
                    </div>
                   
                </nav>
            </div>
        @yield('content')
        </div>
        <script>
            
        </script>
        <script src="{{asset('assets/js/jquery-3.4.1.min.js')}}" crossorigin="anonymous"></script>
        <script src="{{asset('assets/js/bootstrap.bundle.min.js')}}" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
        <script src="{{asset('assets/js/scripts.js')}}"></script>
        <script src="{{asset('assets/js/Chart.min.js')}}" crossorigin="anonymous"></script>
        <script src="{{asset('assets/js/chart-area-demo.js')}}"></script>
        <script src="{{asset('assets/js/chart-bar-demo.js')}}"></script>
        <script src="{{asset('assets/js/jquery.dataTables.min.js')}}" crossorigin="anonymous"></script>
        <script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}" crossorigin="anonymous"></script>
        <script src="{{asset('assets/js/all.min.js') }}" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
        <script src="{{asset('assets/js/datatables-demo.js')}}"></script>
    </body>
</html>
